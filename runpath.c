/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.4 92/07/12
 */

#ifndef lint
static char SCCSid[] = "@(#)runpath.c 1.4 92/07/12 16:43:27";
#endif
#define	UNPACKMAPS
#include "unpack.h"

int stunlinked = 0;
int stprocessed = 0;

dorunpath() {
    extern char *zfgets();
    extern FILE *zfopen();
    struct stat stb;
    register char *p;
    register int recno;
    FILE *list, *mapf, *path, *whfp;
    char buf[BUFSIZ];
    long pathtime = 0;
    char **mapptr, **locptr;

    fatal(chdir(unpackdir), "Can't change to unpacking directory");

    (void) umask(022);

    (void) unlink(pathtmp);
    (void) unlink(pathtmp2);
    (void) unlink(wheretmp);

    /* awwww, do we really have to run it?
       these tests are to confirm that we have to run pathalias
       (if forcepath = 0)
     */

    if (!forcepath) {
	if (stat(wheredb, &stb))
	    forcepath = 1;		/* no where file, run it. */
	else
	    pathtime = stb.st_mtime;
	if (!whereonly) {
	    if (stat(pathfile, &stb))
		forcepath = 1;		/* no path file, run it. */
	    else /* minimum time is trigger */
		pathtime = stb.st_mtime < pathtime ? stb.st_mtime: pathtime;
	}
    }

    if (debug)
	(void) fprintf(stderr, "in dorunpath: pathtime: %ld, force: %d\n",
	    pathtime, forcepath);

    getmaps(&mapfiles, 1);
    for (mapptr = mapfiles.list; mapptr && *mapptr; mapptr++)
	if (stat(*mapptr, &stb))
	    (void) fprintf(stderr, "Can't stat %s\n", *mapptr);
	else {
	    if (pathtime < stb.st_mtime)	/* map file newer, run it */
		forcepath = 1;
	    if (debug)
		(void) fprintf(stderr, "check %s, t: %ld, force: %d\n", buf,
		    stb.st_mtime, forcepath);
	}

    /* then, collect the map file mentioned in the command line */
    recno = mapfiles.curptr - mapfiles.list;

    for (mapptr = localmaps.list; mapptr && *mapptr; mapptr++)
	if (stat(*mapptr, &stb))
	    (void) fprintf(stderr, "Can't stat %s\n", *mapptr);
	else {
	    if (pathtime < stb.st_mtime)	/* local map newer, run it */
		forcepath = 1;
	    savestr(&mapfiles, *mapptr);
	}

    /* pointers >= locptr are command line maps */
    locptr = mapfiles.list + recno;

    if (debug) {
	(void) fprintf(stderr, "finish: forcepath: %d\n", forcepath);
	for (mapptr = mapfiles.list; mapptr && *mapptr; mapptr++)
	    (void) fprintf(stderr, "map %s%s\n", *mapptr,
		mapptr >= locptr? "*": "");
    }


    if (!forcepath)
	return;

    /* only doing it one step at a time.  If we make the popen
       multistage (sed reorder + sort), then we lose the ability
       to replace the sed with something faster, *and* the ability
       to check for errors in each step.

       This should really be forked, with output to pathalias
       on one side, and input from pathalias and output to
       sort on the other.  Then we don't have to have *two*
       path temporaries.  Next time...
     */

    if (whereonly)
	path = (FILE *) NULL;
    else {
	(void) sprintf(tempbuf, "%s > %s", pathalias, pathtmp);
	fatal(!(path = popen(tempbuf, "w")), "Can't popen pathalias");
	(void) fprintf(stderr, "Starting pathalias\n");
    }

    fatal(!(whfp = fopen(wheretmp, "w")), "Can't open where.tmp");

    /* Here we GOOOOOOOO!!!! */

    (void) fflush(stderr);

    for (mapptr = mapfiles.list; mapptr && *mapptr; mapptr++) {
	int writing = 1;	/* set to false if not pumping to pathalias */
	int skipmap;
	char *((*rdfcn)());
	int compressed = 0;

	(void) strcpy(tempbuf, *mapptr);

	p = *mapptr + strlen(*mapptr) - 2;

	if (*p == '.' && *(p+1) == 'Z') {
	    compressed = 1;
	    tempbuf[p - *mapptr] = '\0';
	}

	skipmap = (mapptr < locptr) && search(lmapfiles.list, tempbuf);
	if (!path || skipmap)
	    writing = 0;
	
	if (unlinkskip && skipmap && **mapptr == 'u') {
	    if (verbose)
		(void) fprintf(stderr, "Unlinking %s\n", tempbuf);
	    stunlinked++;
	    (void) unlink(*mapptr);
	    continue;
	}

	if (debug)
	    (void) fprintf(stderr, "opening %s, .Z=: %d, writing: %d, skip: %d\n", *mapptr,
		compressed, writing, skipmap);

	if (compressed) {
	    fatal(!(mapf = zfopen(*mapptr, "r")), "Can't zfopen map file");
	    rdfcn = zfgets;
	} else {
	    fatal(!(mapf = fopen(*mapptr, "r")), "Can't fopen map file");
	    rdfcn = fgets;
	}
	stprocessed++;

#ifndef	OLDPATHALIAS
	if (writing && path)
	    (void) fprintf(path, "file {%s}\n", *mapptr);
#endif
	recno = 0;

	while(rdfcn(buf, sizeof(buf), mapf)) {
	    recno++;

	    /* optimization: delete comments from pathalias input. */
	    if (buf[0] == '#') {
		if (writing)
		    (void) putc('\n', path);

		/* If a #N[ame] record, save information */
		if (buf[1] == 'N') {

		    /* #N[ame][ \t]+site[, ]+site[, ]+,site */
		    for (p = buf + 2; *p && !isspace(*p); p++)
			continue;
		    while(*p && isspace(*p)) p++;
		    while(*p && *p != '\n') {

			while(*p && *p != ',' && !isspace(*p)) {
			    (void) putc(*p, whfp);
			    p++;	/* ya never know */
			}

			(void) fprintf(whfp, "\t%s\t%d\n", tempbuf, recno);

			while(*p && (*p == ',' || isspace(*p)))
			    p++;
		    }
		}
	    } else {
		p = strchr(buf, '#');
		if (p) {
		    for (p--; p >= buf && (*p == ' ' || *p == '\t'); p--)
			continue;
		    if (p < buf) {
			*++p = ' ';	/* wierd but true - pathalias */
			*++p = '#';	/* needs it sometimes */
		    }
		    *++p = '\n';
		    *++p = '\0';
		}
		if (writing)
		    (void) fputs(buf, path);
	    }
	}

	if (compressed)
	    (void) zfclose(mapf);
	else
	    (void) fclose(mapf);
	if (skipmap && unlinkskip && **mapptr == 'u') {
	    (void) fprintf(stderr, "Unlinking %s\n", *mapptr);
	    (void) unlink(*mapptr);
	}
#ifndef	OLDPATHALIAS
	if (writing && path)
	    (void) fprintf(path, "private {}\n");
#endif
    }

    if (stunlinked)
	(void) fprintf(stderr, "%d map files discarded by discard list\n",
	    stunlinked);
    (void) fprintf(stderr, "%d map files processed\n", stprocessed);

    (void) zfiofree();

    (void) fclose(whfp);

    (void) fflush(stderr);
    if (path && pclose(path)) {
	(void) unlink(pathtmp);
	fatal(1, "pathalias failed");
    }

    if (!whereonly)
	(void) fprintf(stderr, "Pathalias completed: okay\n");

    sortwdb();

    (void) fprintf(stderr, "%s completed: okay\n", wheredb);
    if (whereonly)
	return;

    (void) fprintf(stderr, "Sorting pathalias output\n");
    fatal(!(whfp = fopen(pathtmp, "r")), "reopen path temp");
    (void) sprintf(tempbuf, "sort -u > %s", pathtmp2);

    fatal(!(path = popen(tempbuf, "w")), "popen sort");
    (void) unlink(pathtmp);		/* we don't have to do cleanup later */

    /* swap fields around */
    while(fgets(buf, sizeof(buf), whfp)) {
	char *site, *route, *t;

	site = strchr(buf, '\t');
	*site++ = '\0';
	if (route = strchr(site, '\t'))
		*route++ = '\0';
	else
		{
			route = site;
			site = buf;
		}
	t = strchr(route, '\n');
	*t = '\0';

#ifdef	NODOTDOMAIN
	if (*site == '.')
	    continue;
#endif

	/* lcasep's job */
	for (t = site; *t; t++)
	    if (isupper(*t))
		*t = tolower(*t);

	if (internet)
	    dointernet(site, &route);

	(void) fputs(site, path);
	(void) putc('\t', path);
	(void) fputs(route, path);
	if (site != buf)
	{
		(void) putc('\t', path);
		(void) fputs(buf, path);
	}
	(void) putc('\n', path);
    }

    (void) fclose(whfp);
    (void) unlink(pathtmp);
    if (pclose(path)) {
	(void) unlink(pathtmp2);
	fatal(1, "sort failed");
    }
    (void) fprintf(stderr, "Sorting pathalias output completed: okay\n");

    fatal(!(whfp = fopen(pathtmp2, "r")), "Open of pathtmp2 for copy failed");
    fatal(!(path = fopen(pathfile, "w")), "Open of path output file failed");
    while((recno = fread(tempbuf, sizeof(char), BUFSIZ, whfp)) > 0)
	fatal(recno != fwrite(tempbuf, sizeof(char), recno, path),
	    "write failed");
    (void) fclose(whfp);
    (void) fclose(path);
    (void) fprintf(stderr, "Creation of %s completed: okay\n", pathfile);

}

search(list, item)
register char **list, *item; {
    if (debug)
	(void) fprintf(stderr, "find %s\n", item);
    if (!list)
	return(0);
    while(*list)
	if (strcmp(*list++, item) == 0)
	    return(1);
    return(0);
}
