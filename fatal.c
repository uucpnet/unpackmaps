/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.3 92/08/14
 */

#ifndef lint
static char SCCSid[] = "@(#)fatal.c 1.3 92/08/14 20:48:32";
#endif
#define	UNPACKMAPS
#include "unpack.h"

fatal(code, msg)
int code;
char *msg; {

    if (code) {
	(void) fprintf(stderr, "%s: %s\n", progname, msg);
	(void) myexit(code);
    }
}

static char errlog[] = "/tmp/unpXXXXXX";
extern char *mktemp();
startlog() {
    if (!(freopen(mktemp(errlog), "w", stderr))) {
	(void) printf("%s: Can't open temporary file %s for errors!\n",
	    progname, errlog);
	exit(1);
    }
}

myexit(code)
int code; {
    struct stat stb;
    FILE *diag;
    FILE *notifyfp;

    (void) unlink(pathtmp);
    (void) unlink(pathtmp2);
    (void) unlink(wheretmp);
    (void) fclose(stderr);

    if (batchlock)
	setbatch(0);

    if (!notify)
	exit(code);

    if (stat(errlog, &stb) == 0 && stb.st_size > 0) {

	if (!(notifyfp = popen(notify, "w"))) {
	    (void) printf("%s: Can't open mail pipeline ``%s''\n", progname, notify);
	    (void) exit(1);
	}

	(void) fputs("Subject: Map Unpacking Report\n\n", notifyfp);

	if (!(diag = fopen(errlog, "r"))) {
	    (void) printf("%s: Can't open error log %s\n",
		 progname, errlog);
	    (void) exit(1);
	}

	while(fgets(spooldir, sizeof(spooldir), diag)) {
	    (void) fputs(spooldir, notifyfp);
	}
	(void) fclose(diag);
	(void) pclose(notifyfp);
    } 
    (void) unlink(errlog);
    (void) exit(code);
}
