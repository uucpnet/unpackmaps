/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.1 92/06/10
 */

#ifndef lint
static char SCCSid[] = "@(#)sortwdb.c 1.1 92/06/10 01:16:23";
#endif
#define	UNPACKMAPS
#include "unpack.h"
sortwdb() {
    char buf[BUFSIZ];
    char lastsite[BUFSIZ], lastfile[BUFSIZ], lastrecord[BUFSIZ];
    register char *site, *file, *record, *t;
    FILE *in, *out;

    (void) sprintf(tempbuf, "sort -u %s", wheretmp);
    fatal(!(in = popen(tempbuf, "r")), "Can't popen sort of wheretmp");
    (void) unlink(wheredb);
    if (debug)
	(void) fprintf(stderr, "wheredb: %s\n", wheredb);
    fatal(!(out = fopen(wheredb, "w")), "Can't open where.db");

    lastsite[0] = lastfile[0] = lastrecord[0] = '\0';

    while(fgets(buf, sizeof(buf), in)) {
	site = buf;
	file = strchr(site, '\t');
	*file++ = '\0';
	record = strchr(file, '\t');
	*record++ = '\0';
	t = strchr(record, '\n');
	*t = '\0';

	if (strcmp(site, lastsite)) {
	    if (lastsite[0])
		(void) putc('\n', out);
	    (void) fprintf(out, "%s\t%s,%s", site, file, record);
	    (void) strcpy(lastsite, site);
	    (void) strcpy(lastfile, file);
	    (void) strcpy(lastrecord, record);
	} else if (strcmp(file, lastfile)) {
	    (void) fprintf(out, ":%s,%s", file, record);
	    (void) strcpy(lastfile, file);
	    (void) strcpy(lastrecord, record);
	} else if (strcmp(record, lastrecord)) {
	    (void) fprintf(out, ",%s", record);
	    (void) strcpy(lastrecord, record);
	}

    }
    (void) putc('\n', out);

    (void) fclose(out);
    fatal(pclose(in), "Popen'd where.db sort failed");
    (void) unlink(wheretmp);
}
