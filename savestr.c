/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.2 92/08/14
 */

#ifndef lint
static char SCCSid[] = "@(#)savestr.c 1.2 92/08/14 20:48:38";
#endif
#define	UNPACKMAPS
#include "unpack.h"

#define	INCR	5

savestr(l, s)
struct stringlist *l;
char *s; {
    int count;
    char **save;

    if (!l->list || l->curptr >= l->lastalloc) {
	count = (l->lastalloc - l->list) + 1 + INCR;
	save = l->list;
	if (l->list) {
	    l->list = (char **) realloc((char *)l->list, sizeof(char **) * count);
	    l->curptr = (l->curptr - save) + l->list;
	} else {
	    l->list = (char **) malloc(sizeof(char **) * count);
	    l->curptr = l->list;
	}
	fatal(!l->list, "string list allocate - out of memory");
	l->lastalloc = l->list + count - 1;
    }

    *l->curptr = (char *) malloc(strlen(s)+1);
    fatal(!*l->curptr, "string list string allocate - out of memory");
    (void) strcpy(*l->curptr, s);
    l->curptr++;
    *l->curptr = (char *) NULL;
}
