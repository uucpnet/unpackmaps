/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.5 92/08/14
 */

#undef BSD		/* define if you're a BSD or V7 system */
#define USG		/* define if you're a System V or Xenix system */
#define NEEDTYPES_H	/* define if your sys/stat.h needs types.h,
			   but doesn't already include it. */

#define MAPDIR		"/usr/spool/maps"

/*	Default pathalias invocation line.
	Should have -i to lower case everything.
	If you're running smail 2.5, which likes to see the cost
	of the first hop, supply a -f option.
 */
#define PATHALIAS	"/usr/bin/pathalias -i -f"

#define SPOOLDIR	"/usr/spool/news"
#define PATHFILE	"/usr/lib/uucp/paths"

/*	undef COMPFLAG if you want COMPRESS's default compress ratio.
	-b16 uses up about 450K.
	-b12 uses up about 60K. */

#define COMPFLAG	"-b12"
#define COMPRESS	"/usr/bin/compress"

#define NEWSID		"news"	/* *must* be same as news system owner. */

#define MALLRET		char *	/* what malloc() and realloc() return
				   sometimes "void *" */


/*	Where you're likely to have the most space.  If you have several
	smallish partitions, it would make sense to have these different.
	Eg: /tmp and /usr/tmp.  unpackmaps attempts to balance between
	the two.  In fact, if these are on different spindles, this
	may improve performance somewhat as well.
 */
#define TMPDIR		"/tmp"
#define TMPDIR2		"/tmp"

/*	Bnews doesn't need to lock batch files.  Cnews *does*.
	If you're not running Cnews, undefine LOCKBATCH.
	If you are running Cnews, make sure that LOCKBATCH
	is set to the lock file name.  Usually /usr/lib/news/LOCK.
 */
#define	LOCKBATCH	"/usr/lib/news/LOCK"

/*	If you have SVR[34] or Posix-compliant directory routines,
	define "USGDIR". If you have Berkeley directory routines
	(usually older systems) define BERKDIR. If you have
	neither, or are unsure, leave both undefined, and my
	ugly and slow "ls" kludge will work.
 */
#define USGDIR
#undef BERKDIR

/*	Turn this off if you're running smail 3, or smail 2.5 with the
	"dotteddomain" bug fixed
 */

#define	NODOTDOMAIN

/*	Unpackmaps will insert "file" directives into the beginning of
	each map when stuffing it into pathalias so any error messages
	will have the right file name and line number.  If OLDPATHALIAS
	is defined, unpackmaps will not do this.  Older versions of
	pathalias (9 and previous) don't grok the "file {name}" directive.
	Rutgers has just recently started sending maps with the file 
	directive in them - so, theoretically, as long as you have the
	new pathalias, you could define this to disable this feature.
	But, it doesn't hurt, and makes sure that the file directive is
	inserted even on local maps where you may have forgotten to insert
	the file directive.

	Defining this doesn't cause unpackmaps to strip already-existing
	"file" directives from the maps - so you can't use this to
	adapt the new maps to old pathalias.  It's far more important
	to upgrade to the new pathalias (10 or better).  Older pathalias
	tends to go into infinite loops in some obscure situations.
 */

#undef	OLDPATHALIAS

/*	Some systems have a maximum file length of 14.  Ie: System V
	on the System-V file system type (as opposed to the Berkeley
	Fast File System for instance.)  Occasionally, Rutgers will
	goof and send out maps with file names longer than 12 (which
	will cause a problem on System V systems when you try to
	compress - compress adds 2 characters for the .Z).  Rutgers
	then usually resends the map with a new length of 12.

	If FILELENGTH is set to:

	    0		unpackmaps will refuse to unpack anything longer
			than 12.
	    1		unpackmaps will unpack files with length of 13 or 14,
			but not compress them.
	    nn		unpackmaps will unpack as long as the file name
			length is <= nn, and compress if <= nn-2.

	I strongly recommend a setting of 0, and whenever unpackmaps tells
	you that a map is longer than 12, send a complaint off to
	uucpmap@rutgers.
 */

#define FILELENGTH	0
