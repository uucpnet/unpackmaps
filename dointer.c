/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.2 92/11/28
 */

#ifndef lint
static char SCCSid[] = "@(#)dointer.c 1.2 92/11/28 00:44:13";
#endif
#define	UNPACKMAPS
#include "unpack.h"

int
countbang(path)
register char *path; {
    register int i;
    for (i = 0; *path; path++)
	if (*path == '!')
	    i++;
    return(i);
}

dointernet(site, route)
register char *site, **route; {
    static int intbangcount = -1;
    register char *p;
    
    if (intbangcount == -1)
	intbangcount = countbang(internet);

    if (*site == '.') {
	/* .domain longpath!%s -> .domain internet!%s */
	if (intbangcount + 2 < countbang(*route)) {
	    (void) sprintf(tempbuf, "%s!%%s", internet);
	    *route = tempbuf;
	}
    } else if (strchr(site, '.')) {
	/* domain longpath!%s -> domain internet!domain!%s */
	if (intbangcount + 2 < countbang(*route)) {
	    (void) sprintf(tempbuf, "%s!%s!%%s", internet, site);
	    *route = tempbuf;
	}
    } else if (p = strrchr(*route, '.')) {
	/* any longpath!domain!path!%s -> any internet!domain!path!%s */
	while(p > *route && *p != '!') p--;
	if (*p == '!') {
	    *p = '\0';
	    if (intbangcount < countbang(*route)) {
		(void) sprintf(tempbuf, "%s!%s", internet, p+1);
		*route = tempbuf;
	    }
	    *p = '!';
	}
    }
}
