/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.2 92/08/14
 */

#ifndef lint
static char SCCSid[] = "@(#)docomp.c 1.2 92/08/14 20:48:28";
#endif
#define	UNPACKMAPS
#include "unpack.h"
#define	ARMAX	10

#ifdef	COMPFLAG
#define	FF	2
#else
#define	FF	1
#endif

/*	Buffers up files to compress in big chunks */
docompress(file)
register char *file; {
    char tb[16];
    static char *arglist[ARMAX + 3] = {"COMPRESS"
#ifdef	COMPFLAG
	, COMPFLAG
#endif
    };
    static char **ap = &arglist[FF];

    /* If maps are coming in two different newsgroups (not x-posted),
       it is possible that you'll get the same name twice.  If you
       don't ignore the duplicates, compress will complain that it
       couldn't find the subsequent instances.  While this is harmless,
       it is somewhat disturbing.  We simply ignore the
       second or subsequent mention of the same file name.

       [The scenario happens with the Canadian maps.  Both Rutgers and
       the Canadian map authority post the Canadian maps simultaneously.
       The former in comp.mail.maps, the latter in can.uucp.maps.
       The logic behind this is relatively sound, but too long to reproduce
       here.  They're not cross-posted because it breaks uuhosts (but not
       unpackmaps! ;-).]
     */

    if (file && ap > &arglist[FF]) {
	char **p;
	
	for (p = &arglist[FF]; p < ap; p++)
	    if (strcmp(file, *p) == 0)
		return;
    }

#if	(FILELENGTH == 0) || (FILELENGTH == 1)
# define MAXCOMPLEN 12
#else
# define MAXCOMPLEN (FILELENGTH - 2)
#endif

    /* To avoid compress complaints on systems with file name restrictions */
    if (file && strlen(file) > MAXCOMPLEN)
	return;

    if (file) {
	(void) strcpy(tb, file);
	(void) strcat(tb, ".Z");
	(void) unlink(tb);
    }

    if (!compflag)
	return;

    if (debug)
	(void) fprintf(stderr, "docompress %s (%d)\n", file, ap - arglist);

    if ((!file || ap > &arglist[ARMAX]) && ap > &arglist[FF]) {
	int pid, wpid, waitloc;
	if (debug)
	    (void) fprintf(stderr, "Firing off compress (fc = %d)\n",
		ap - arglist);

	if (debug)
	    for (ap = arglist; *ap; ap++)
		(void) fprintf(stderr, "%d: %s\n", ap-arglist, *ap);

	fflush(stderr);
	if ((pid = fork()) == 0) {
	    (void) execv(compress, arglist);
	    fatal(1, "Can't exec compress");
	}
	while((wpid = wait(&waitloc)) != pid && wpid > 0)
	    continue;
	if (waitloc) {
	    (void) fprintf(stderr, "%s: compress failed, returned 0x%x (pid: %d)\n",
		progname, waitloc, wpid);
	}
	for (ap = &arglist[FF]; *ap; ap++)
	    free(*ap);
	ap = &arglist[FF];
    }
    if (file) {
	*ap = (char *) malloc(strlen(file)+1);
	(void) strcpy(*ap++, file);
	*ap = (char *) NULL;
    }
}
