/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.1 92/06/10
 */

#ifndef lint
static char SCCSid[] = "@(#)lock.c 1.1 92/06/10 01:16:19";
#endif

#define UNPACKMAPS
#include "unpack.h"

#define	TRYTIME

int
setbatch(on)
int on; {
#ifdef	LOCKBATCH
    batchlock = 1;
    return(mylock(LOCKBATCH, on));
#else
    return(0);
#endif
}

#define INTERVAL	25
int
mylock(lockfile, on)
char *lockfile;
int on; {
    if (!on) {
	(void) unlink(lockfile);
    } else {
	register char *tempnm, *p;
	FILE *tempfp;
	int locktries = 10;

	fatal(!(tempnm = (char *) malloc(strlen(lockfile) + 15)),
	    "allocate temp lock");
	(void) strcpy(tempnm, lockfile);
	fatal(!(p = strrchr(tempnm, '/')), "Lock isn't full path");
	*p = '\0';
	(void) strcat(tempnm, "/LCKXXXXXX");
	(void) mktemp(tempnm);

	fatal(!(tempfp = fopen(tempnm, "w")), "Can't create temp lock");
	(void) fprintf(tempfp, "%d\n", getpid());
	(void) fclose(tempfp);

	while (link(tempnm, lockfile) < 0) {
		fatal(errno != EEXIST, "Can't link locks");
		fatal(!(locktries--), "Couldn't get lock");
		(void) sleep(INTERVAL);
	}
	(void) unlink(tempnm);
	free(tempnm);
    }
    return(0);
}
