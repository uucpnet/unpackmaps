/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.2 92/08/14
 */

#ifndef lint
static char SCCSid[] = "@(#)uuwhere.c 1.2 92/08/14 20:48:55";
#endif
#define	INIT(x)	= x
#include "unpack.h"

int verbose;
int lflag;
int emitmap;

int rc = 0;

main(argc, argv)
int argc;
char **argv; {
    int cost;
    register int i;
    char buf[BUFSIZ];
    char smarthost[BUFSIZ];
    int smarthosted;
    int c;
    char *region = NULL;
    register char **p, *rp;
    extern char *optarg, *gpathdata;
    extern int optind;
    extern long pathlength;

    progname = argv[0];

    while((c = getopt(argc, argv, "Xd:m:evr:l")) != EOF)
	switch(c) {
	    case 'd':
		(void) strcpy(unpackdir, optarg);
		break;
	    case 'm':
		(void) strcpy(pathfile, optarg);
		break;
	    case 'v':
		verbose = 1;
		break;
	    case 'e':
		emitmap = 1;
		break;
	    case 'l':
		lflag = 1;
		break;
	    case 'r':
		region = optarg;
		break;
	    case 'X':
		debug = 1;
		break;
	    case '?':
		usage = 1;
		break;
	}

    if (usage) {
	static char *uuse[] = {
	    "usage: uuwhere <options>",
	    "  options: -d mapdir      map directory",
	    "           -m pathfile    pathalias map file",
	    "           -e             display map entry)",
	    "           -v             display interesting path information",
	    "           -l             display map file names",
	    "           -r region      display map (region) file",
	    NULL
	};

	for (p = uuse; *p; p++)
	    (void) fprintf(stderr, "%s\n", *p);

	exit(1);
    }


    if (chdir(unpackdir)) {
	(void) fprintf(stderr, "%s: Can't change to %s\n", progname, unpackdir);
	exit(1);
    }

    wheredb = makepath(unpackdir, "where.db");
    if (debug)
	(void) fprintf(stderr, "wheredb: %s\n", wheredb);

    if (lflag)
	exit(dolist());

    if (region) {
	int list[2];
	list[0] = 1;
	list[1] = 0;
	exit(dumpfile(region, list, 0));
    }

    gpathdata = pathfile;
    pathlength = 0;

    if (getpath("smart-host", smarthost, &cost))
	smarthost[0] = '\0';
    
    for (i = optind; i < argc; i++) {
	buf[0] = 0;
	smarthosted = 0;
	if (emitmap)
	    pathlength = 0;
	if (getpath(argv[i], buf, &cost)) {
	    if (smarthost[0]) {
		(void) sprintf(buf, smarthost, argv[i]);
		smarthosted = 1;
		rc |= 2;
	    } else {
		buf[0] = '\0';
		rc |= 4;
	    }
	} else {
	    rp = strrchr(buf, '!');
	    if (rp)
		*rp = '\0';
	    else
		(void) strcpy(buf, argv[i]);
	}

	if (verbose) {
	    (void) printf("route %s: %s!<user>", argv[i], buf[0]? buf: "<noroute>");
	    if (smarthosted)
		(void) printf(" (smarthosted)");
	    (void) putchar('\n');
	} else
	    if (buf[0])
		(void) printf("%s\n", buf);
	if (emitmap)
	    (void) dumpmap(argv[i]);
    }
    exit(rc);
    /* NOTREACHED */
}

struct stringlist sl;
dolist() {
    register char *p, *q;
    char **mapptr;
    struct stat stb;
    extern char *ctime();

    getmaps(&sl, 0);
    for (mapptr = sl.list; *mapptr; mapptr++)
	if (verbose) {
	    if (stat(*mapptr, &stb))
		(void) fprintf(stderr, "%s: can't stat %s\n", progname, *mapptr);
	    else {
		p = ctime(&stb.st_mtime);
		q = strchr(p, '\n');
		if (q)
		    *q = '\0';
		(void) printf("%-14s %6ld %s\n", *mapptr, stb.st_size, p);
	    }
	} else {
	    q = strrchr(*mapptr, '.');
	    if (q && strcmp(q, ".Z") == 0)
		*q = '\0';
	    (void) printf("%s\n", *mapptr);
	}
    return(0);
}

fatal(e, str)
int e;
char *str; {
    if (e) {
	(void) fprintf(stderr, "%s: %s\n", progname, str);
	exit(e);
    }
}
