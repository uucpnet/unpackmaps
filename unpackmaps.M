.\"Copyright 1992 by Chris Lewis 1.5 92/11/28
.TH UNPACKMAPS 1 "Chris Lewis"
unpackmaps \- USENET UUCP map unpacker
.SH SYNOPSIS
.B unpackmaps
.BI [ options ]
.SH DESCRIPTION
.B Unpackmaps
is a specially optimized hack program for retrieving and
safely unpacking the UUCP maps produced by the UUCP Mapping Project.
It can be instructed to run pathalias to generate the UUCP routing
table that various other mailers need (ie: smail 2.5, IDA Sendmail).
It will also create a special database that
.BR uuwhere (1)
uses to display the map entries for individual sites.
.P
.B Unpackmaps
can also be used to unpack other USENET postings, such as Brian
Reid's maps of USENET sites (news.lists.ps-maps), provided that the
shar format used conforms to the UUCP Map postings.
.P
One of the difficulties inherent in most posted map unpackers is
that they use a ``real'' unshar to unpack map articles.
Since a ``real'' unshar is just invoking the shell on a portion
of an article, this can easily be seen to be an
.BR "extreme security hole" .
Which bears repeating, using an insecure (usually home grown) unpacker
is just asking for your machine to be
.BR destroyed .
.P
The ``SECURITY ISSUES'' section of this manual page analyses
some of the potential vulnerabilities, and discusses how
.B unpackmaps
deals with them.
.P
The term ``hack'' was used in the first paragraph.
.B Unpackmaps
is not a ``tool'' in the UNIX sense.
The reason for this is simple: the maps are huge, and some machines
are quite small.
.B Unpackmaps
is designed to use as little time, memory and disk space as possible.
In fact,
.B Unpackmaps
will allow you to compress the maps and run pathalias even on 16 bit machines,
provided that you instruct it to use a sufficiently small subset of the maps
that
.B pathalias
doesn't run out of memory.
.P
The highest load that 
.B unpackmaps
imposes is during the map decompression
and pathalias phase.
.B Unpackmaps
contains its own decompression function that dynamically sizes itself
to the requirements of the compressed files.
If the maps are compressed with 16 bit compression (a build option),
.B unpackmaps
will grow to around 500K.
Processing the complete UUCP maps currently (March 1992) causes
.B pathalias
to grow to about 2Mb.
.B Unpackmaps
will not page or swap provided that you have enough memory to fit it
and
.BR pathalias .
It is suggested that you consider reducing the compression factor in
config.h to prevent things from swapping to disk.
.SH "BASIC FUNCTIONALITY"
.B Unpackmaps
does all of its work in a directory set aside for the purpose.
It should be created and it's ownership should be set to the
userid that owns your news system.
This is the ``map directory''.
There should be a different map directory for each different
type of article you wish to unpack.
The sys file for your news system is instructed to write
the file names of each incoming article in the desired
groups into a
.I togo
file in the appropriate directory.
.P
.B Unpackmaps
is run from a cron job, as ``news'', and will read through
the 
.I togo
file and unpack the map articles into the directory.
.P
If instructed,
.B unpackmaps
will invoke pathalias on the maps and generate a paths
database and
.B uuwhere
database.
This can be either unconditional, or based on whether any
articles were unpacked.
.P
Optionally, the unpacked articles can be compressed,
and the original articles removed from the spool area
as they are being extracted.
.SH OPTIONS
.P
Unpacking options:
.TP .5i
.B \-V
Print unpackmaps version.
.TP .5i
.B \-v
Print file names of maps and map names as they're unpacked, discarded
or unlinked.
.TP .5i
.B \-t
Skip unpacking step.
Also prevents unpackmaps from insisting on running as the NEWSID.
Which permits invocation as an arbitrary user, but care needs to be
taken that you specify map files in places that are writable by
you.
.TP .5i
.BI \-n notify
Specifies a mail pipeline to do notification of completion.
If not specified, all output is on stderr.
.TP .5i
.BI \-d directory
Directory to do the unpacking in.
Default:
.IR /usr/spool/maps .
.TP .5i
.B \-u
Delete the articles from the news system after they're unpacked.
Do not do this unless you don't pass these articles on to your neighbors,
ie: you are a leaf site.
.TP .5i
.B \-c
Compress the files after they're created.
.B Compress
is invoked with groups of 10 files apiece.
.TP .5i
.B \-i
Initialize - this is a short cut for starting up
.B unpackmaps
when you have an empty map directory, and a news spool directory
full of articles.
It merely creates a
.I togo
file from the contents of
.BI <spooldir> /comp/mail/maps.
This can easily be done via a manual
.in +.5i
.nf
.sp
ls <full path to a directory>/* >> <mapdir>/togo
.sp
.fi
.in -.5i
.TP .5i
.BI \-s directory
Set the news spool directory.
Default:
.IR /usr/spool/news .
This is required for proper unpacking if your system does not use
.I /usr/spool/news
for the news spool directory, or your news system doesn't write
full paths into the togo files (ie: new versions of C news).
.P
Pathalias invocation options:
.TP .5i
.B \-P
Run pathalias.
.TP .5i
.B \-p
Run pathalias if any of the maps are newer than the last UUCP map created.
.TP .5i
.BI \-M pipeline
Alternate pathalias pipeline.
Implies \-p.
This should really be placed in a shell script.
Standard input is the maps themselves (comments are stripped out
at this point), and standard output is the
pathalias file.
For example, something like this could be used:
.P
.in +.5i
.nf
/usr/local/bin/pathalias -fi | sed -e 's/foo!bang/bang!foo/'
.fi
.in -.5i
.P
Default specified in compile.
With the -f option, pathalias will be emitting the paths in this form:
.P
.nf
cost   site   route
.fi
.P
.TP .5i
.BI \-m file
Specifies where the resultant UUCP map should be written.
Implies
.BR \-p .
Default:
.I /usr/lib/uucp/paths
(Smail 2.5 default)
.TP .5i
.BI \-f file
Append
.I file
onto the list of map directory files to be processed through pathalias.
This is how you specify local map files.
This option can be used more than once.
The file names should be full paths, unless you keep the file in the
map directory.
In which case, they should all be named ``path.<something>'' (see
``SECURITY ISSUES'' section).
.TP .5i
.B \-W
Just create the
.B uuwhere
database
.RI ( where.db ),
and don't run
.BR pathalias .
.TP .5i
.BI \-w
Same as \-W, except don't create the where database unless any
of the maps are newer than when it was last created.
.TP .5i
.B \-U
Unlink any ``u.''-prefixed map file mentioned in the
.BR ignorefile .
.TP .5i
.BI \-l file
Specify the
.BR ignorefile .
A file listing, one per line, the map files that should not be
piped into
.BR pathalias .
If
.B \-l
is used without
.BR \-U ,
.B unpackmaps
will not pass the files mentioned in the ignorefile through to
.BR pathalias ,
however, all files will be cross-referenced in the
.B uuwhere
database.
This is convenient for keeping a database of all sites in the map, but
only constructing a
.B paths
file for a subset of the net.
(See the OPERATIONS section for an example).
.IP "" .5i
If \-U is also specified, any file listed in the
.B ignorefile
will be deleted (only if it has a ``u.'' prefix), and not cross-referenced in the
.B where
database.
.TP .5i
.BI \-I route
If you have a nearby friendly
site with good routing software, you can specify a route here.
This will replace any path entries for sites with domain names with
a route to that site followed by the domain site.
For example,
.BI \-I foo!uunet.ca
will cause
.B unpackmaps
to replace all routes to domain-named sites with
.IR "foo!uunet.ca!<domain-named site>!<user>" .
This is only done if the
.BR pathalias -created
routing has two more hops than the specified
.IR route .
Further,
all ``.domain'' routes
that are longer than the \fIroute\fP
will be shortened to 
.IR "route!<domain>!<user>" .
And finally, if there are domainized sites in a route, the path
to the last domainized site will be replaced with
.IR route .
Ie: ``a!b!c!d.edu!e.edu!d'' will be transformed to
.IR route "!e.edu!d".
.IP "" .5i
This should be done with extreme care, with the permission of all of the
sites involved in the specified
.IR route .
Further, the designated forwarder site \fBMUST\fP support full domain
addressing - hopefully it is running full DNS/BIND on the Internet.
However, properly managed pathalias sites (such as ones running
.B unpackmaps
;-) should be capable of being your internet forwarder.
This is primarily intended for situations where you're directly connected
to a commercial Internet service, such as
.BR uunet .
This is roughly analogous to
.B pathalias
.I smart-host
in that domain-named sites are automatically sent to a specific Internet
site, instead of all mail to sites not listed in the maps.
Note, however, that you don't need to make your internet route be
the same as your smart-host.
.SH "SETUP AND OPERATION"
.P
First of all, you should create a directory for each set of articles
you wish to unpack.
We'll assume the use of /usr/spool/maps for UUCP maps.
This directory should be owned by the same userid that runs news.
We'll assume ``news''.
.P
You will usually need to create the file that
.B unpackmaps
will write the UUCP paths file into, and change its
ownership to news.
Ie:
.P
.in +.5i
.nf
su root
touch /usr/lib/uucp/paths
chown news /usr/lib/uucp/paths
chmod 644 /usr/lib/uucp/paths
.fi
.in -.5i
.P
Then you should insert the following into your news
.I sys
file:
.P
For Cnews:
.P
maps:comp.mail.maps/all:f:/usr/spool/maps/togo
.P
For Bnews:
.P
maps:world,comp.mail.maps:F:/usr/spool/maps/togo
.P
Then, you install a cron entry to invoke unpackmaps.
It should be invoked by the userid that owns news.
If you have the 
.IR /usr/lib/crontab -style
cron you will want to insert this into
.IR /usr/lib/crontab :
.in +.5i
.nf
.sp
m h * * * su news -c "<unpackmaps invocation>"
.sp
.fi
.in -.5i
Where
.B m
and
.B h
are the minute and hour you want the program to run.
.P
If you have a
.BR crontab -style
version of cron, you would enter the same thing into the
news cron entry, but the "su news -c" and double quotes aren't
necessary.
.P
The best approach is to have the crontab entry invoke a shell
script that has the
.B unpackmaps
invocation line.
.P
The "unpackmaps invocation" would normally be something like this
.in +.5i
.nf
.sp
<path to unpackmaps>/unpackmaps -cpn'mail news'
.sp
.fi
.in -.5i
.P
Which simply means compress any incoming maps and run
.BR pathalias .
The
.B uuwhere
database will be created as well.
The results of the run will be mailed to the news userid.
.P
It's usually common for a system administrator to
have a local copy of his site map entry and/or a
map entry that is used to customize/"fix" routing
elsewhere.
Supply these as \-f options to
.BR unpackmaps .
I use
.B path.local
to keep the most up-to-date copy of my map entry,
and
.B path.hacks
to keep local customizations (eg: ``dead'' links etc.)
.P
Systems that are really short of space may want to run
.B unpackmaps
more often.
One suggested approach would be to run the first
.B unpackmaps
entry several times a day,
and the second one once per day:
.sp
.nf
unpackmaps -cuUwl keepfile -n '/bin/mail somebody'
unpackmaps -cuUpl keepfile -n '/bin/mail somebody'
.fi
.P
If you wish to unpack Brian Reid's network maps, try the following
approach:
.P
First, create /usr/spool/ps-maps, owner news.
.P
Modify the sys file thusly:
.P
For Cnews:
.P
ps-maps:news.lists.ps-maps/all:f:/usr/spool/ps-maps/togo
.P
For Bnews:
.P
ps-maps:world,news.lists.ps-maps:F:/usr/spool/ps-maps/togo
.P
Then run the following as news from your cron:
.P
unpackmaps -cud /usr/spool/ps-maps
.SH "SECURITY ISSUES"
.P
It is trivially easy to forge a posting in the map newsgroups.
Many map unpackers more-or-less ram the article straight through
the shell expecting it to be a well-behaved shar file.
On the other hand, the forger could include a ``rm -fr /''
or something even more damaging in the article instead.
.P
.B Uuhosts
takes the approach of isolating the unpacking area
under a ``chroot(2)''.
This is a little awkward, because you have to duplicate
many things into the chroot'd area, and you have to run the
unpacker as root.
.P
.B Unpackmaps
takes an entirely different approach.
The UUCP mapping project (and Brian Reid's ps-maps) uses a
very simple format for encapsulating maps:
.P
.nf
<stuff>
cat << endtoken > file
<body of map>
endtoken
.fi
.P
.B Unpackmaps
scans the file looking for the ``cat'' lines and parses it
in C, coming up with the file name and endtoken.
Thus, it is impossible for a forger to do anything other than
create a file.
.P
.B Unpackmaps
then checks the specified filename for oddities:
a prefix of "../" would specify writing a file outside of the
map directory.
Other possibilities are shell metacharacters (which might cause
the unpacker to foul up), or insertion of shell scripts to snare
the unwary system administrator with ``.'' first in their PATH.
.P
.B Unpackmaps
scans the file name and will reject a file with:
a leading ``.'', or that contains any character that is not
alphabetic (lower and upper case), a digit (0..9) or a period.
It also rejects names that are longer than 14 characters.
.P
Since
.B unpackmaps
also puts other files into the map directory, it also checks
for and rejects attempts to write to ``togo*'', ``where.db''
or ``path.*'' (the latter for SA's to install their
own map entries via \-f).
.P
And finally, all files are created mode 644, so they can't
be executed by accident.
.P
.B Uuhosts
spends a fair amount of time verifying article headers.
Given the ease of forgery, this seems rather pointless.
This checking also makes it difficult to make any changes
to the map headers - such as cross-posting to other groups.
.SH FILES
.br
.if t .ta 2.5i
.if n .ta 3.5i
/usr/spool/maps	Default map directory
"/where.db	Where database
"/maps	Where the unpacked maps are kept
/usr/lib/uucp/paths	Default paths file
.SH AUTHOR
.B Unpackmaps
was written by Chris Lewis.
The built-in decompress routine was written by James A. Wood
and appeared in the 1990 (?) Obfuscated C Contest.
(Though was somewhat de-obfuscated and modified for use in
.BR unpackmaps )
