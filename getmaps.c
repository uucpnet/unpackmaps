/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.1 92/06/10
 */

#ifndef lint
static char SCCSid[] = "@(#)getmaps.c 1.1 92/06/10 01:16:16";
#endif

#include "unpack.h"

#if defined(USGDIR) || defined(BERKDIR)
int
filesort(a, b)
char **a, **b; {
    return(strcmp(*a, *b));
}

void
getmaps(sl, flag)
int flag;
struct stringlist *sl; {
    DIR *dirp;
    struct dirent *dp;
    struct stat stb;

    dirp = opendir(".");
    while((dp = readdir(dirp)) != NULL) {
	if (dp->d_name[0] == '.')
	    continue;
	if (flag && ((dp->d_name[0] != 'd' && dp->d_name[0] != 'u') ||
	    dp->d_name[1] != '.'))
	    continue;
	if (debug)
	    (void) fprintf(stderr, "Adding %s\n", dp->d_name);
	if (stat(dp->d_name, &stb))
	    (void) fprintf(stderr, "Can't stat %s\n", dp->d_name);
	else
	    (void) savestr(sl, dp->d_name);
    }
    (void) closedir(dirp);
    (void) qsort(sl->list, sl->curptr - sl->list, sizeof(char **), filesort);
}
#else
void
getmaps(sl, flag)
struct stringlist *sl;
int flag; {
    char buf[40];
    register char *p;
    FILE *list;
    struct stat stb;

    /* first, collect the files from the directory */
    fatal(!(list = popen("/bin/ls * 2>/dev/null", "r")), "can't popen map list");

    while(fgets(buf, sizeof(buf), list)) {
	if ((p = strchr(buf, '\n')) != NULL)
	    *p = '\0';
	if (flag && ((buf[0] != 'd' && buf[0] != 'u') || buf[1] != '.'))
	    continue;
	if (stat(buf, &stb))
	    (void) fprintf(stderr, "Can't stat %s\n", buf);
	else
	    (void) savestr(sl, buf);
    }

    fatal(pclose(list), "Spool area ls failed");
}
#endif
