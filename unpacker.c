/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.3 92/07/12
 */

#ifndef lint
static char SCCSid[] = "@(#)unpacker.c 1.3 92/07/12 16:33:55";
#endif
#define	UNPACKMAPS
#define	INIT(x)	= x
#include "unpack.h"

#include <signal.h>

quit() {
    fatal(1, "Interrupted");
}

char *
checkabs(value)
char *value; {
    static char pwd[512];
    static char buf[512];
    if (*value != '/') {
	if (!*pwd) {
	    FILE *p;
	    char *bp;
	    p = popen("pwd", "r");
	    if (!p) {
		fprintf(stderr, "%s: can't popen pwd!\n", progname);
		exit(1);
	    }
	    fgets(pwd, sizeof(pwd), p);
	    pclose(p);
	    bp = strchr(pwd, '\n');
	    if (bp)
		*bp = '\0';
	}
	strcpy(buf, pwd);
	strcat(buf, "/");
	strcat(buf, value);
	return(buf);
    } else
	return(value);
}


main(argc, argv)
int argc;
char **argv; {
    extern int optind;
    extern char *optarg;
    int c;

    progname = strrchr(argv[0], '/');
    if (progname)
	progname++;
    else
	progname = argv[0];
    
    
    while((c = getopt(argc, argv, "vM:tn:VI:cf:s:d:uXpPm:iwWl:U")) != EOF)
	switch(c) {
	    case 'V':
		(void) printf("%s\n", VERSION);
		exit(0);
	    case 'v':
		verbose = 1;
		break;
	    case 't':
		dontunpack = 1;
		break;
	    case 'I':
		internet = optarg;
		break;
	    case 'l':
		(void) strcpy(tempbuf, optarg);
		break;
	    case 'U':
		unlinkskip = 1;
		break;
	    case 'W':
		forcepath = 1;
		/*fallthru*/
	    case 'w':
		whereonly = runpath = 1;
		break;
	    case 'c':
		compflag = 1;
		break;
	    case 'i':
		initialize = 1;
		break;
	    case 'f':
		savestr(&localmaps, checkabs(optarg));
		break;
	    case 'X':
		debug = 1;
		break;
	    case 's':
		(void) strcpy(spooldir, checkabs(optarg));
		break;
	    case 'd':
		(void) strcpy(unpackdir, checkabs(optarg));
		break;
	    case 'P':
		forcepath = runpath = 1;
		break;
	    case 'M':
		pathalias = optarg;
		runpath = 1;
		break;
	    case 'm':
		(void) strcpy(pathfile, checkabs(optarg));
	    case 'p':
		runpath = 1;
		break;
	    case 'u':
		unlinkflag = 1;
		break;
	    case 'n':
		notify = optarg;
		break;
	    case '?':
		usage = 1;
		break;
	}

    if (usage) {
	static char *puse[] = {
	    "usage: unpacker <options>",
	    "  options: -f <local map files>",
	    "           -i   (initialize from comp.mail.maps group)",
	    "           -X   (debug)",
	    "           -c   (compress maps)",
	    "           -s <spooldir>",
	    "           -d <map/unpackdir>",
	    "           -m <output map> (implies -p)",
	    "           -M <pathalias pipeline) (implies -p)",
	    "           -p   (run pathalias if paths out of date)",
	    "           -P   (run pathalias unconditionally)",
	    "           -u   (unlink maps after processing)",
	    "           -V   (print unpackmaps version)",
	    "           -v   (verbose diagnostics)",
	    "           -w   (do where file only if out of date)",
	    "           -W   (do where file unconditionally)",
	    "           -U   (unlink maps mentioned in ignorefile)",
	    "           -l <ignorefile>",
	    "           -n <mail command to send output>",
	    "           -t   (don't unpack - ignore newsgroup)",
	    NULL
	};
	register char **p;

	for (p = puse; *p; p++)
	    (void) fprintf(stderr, "%s\n", *p);

	exit(1);
    }

    if (!dontunpack)
	(void) checkids();

    if (SIG_DFL == signal(SIGINT, SIG_IGN))
	signal(SIGINT, quit);
    (void) signal(SIGTERM, quit);

    togofile = makepath(unpackdir, "togo");
    workfile = makepath(unpackdir, "togo.work");
    wheredb = makepath(unpackdir, "where.db");


    /* gets linked, so must be in unpackdir */
    tempfile = makepath(unpackdir, "_temp_");

    if (notify) {

	/* Normally these should be created ala mktemp, but we have
	   to make sure that we clobber on subsequent runs.
	   Otherwise, there goes your disk if something *really*
	   wierd goes wrong and they don't get deleted for some reason. */

	wheretmp = makepath(TMPDIR2, "UNPAwhere");
	pathtmp = makepath(TMPDIR, "UNPApathtmp1");
	pathtmp2 = makepath(TMPDIR2, "UNPApathtmp2");
    } else {
	wheretmp = makepath(TMPDIR2, ".where");
	pathtmp = makepath(TMPDIR, ".pathtmp1");
	pathtmp2 = makepath(TMPDIR2, ".pathtmp2");
    }

    unlink(tempfile);
    unlink(wheretmp);
    unlink(pathtmp);
    unlink(pathtmp2);

    if (tempbuf[0])
	(void) keeplist(tempbuf);

    if (notify)
	(void) startlog();
	
    if (!dontunpack)
	(void) unpack();

    if (runpath)
	(void) dorunpath();

    myexit(0);
    /* NOTREACHED */
}

keeplist(file)
char *file; {
    FILE *f;
    register char *p;
    fatal(!(f = fopen(file, "r")), "Can't open keeplist");

    while(fgets(tempbuf, sizeof(tempbuf), f)) {

	p = strchr(tempbuf, '\n');

	if (p)
	    *p = '\0';

	p = strrchr(tempbuf, '.');
	if (p && 0 == strcmp(p, ".Z"))
	    *p = '\0';

	if (tempbuf[0])
	    savestr(&lmapfiles, tempbuf);
    }
    (void) fclose(f);
}

#include <pwd.h>
checkids() {
    struct passwd *p;
    extern struct passwd *getpwnam();

    fatal(!(p = getpwnam(NEWSID)), "Can't getpwnam(NEWSID)");
    if (geteuid() == 0)
	(void) setuid(p->pw_uid);
    fatal(p->pw_uid != geteuid() || p->pw_uid != getuid(),
	"Must be run as NEWSID");
}
