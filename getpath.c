/* taken from: *sccsid="@(#)getpath.c	2.5 (smail) 9/15/87";
   Permission pending
 */

#ifndef lint
static char SCCSid[] = "@(#)getpath.c 1.1 92/06/10 01:16:17";
#endif

#include "unpack.h"

#define DEBUG	if (debug) (void) printf
char *gpathdata;
#define	lower(x)	(isupper(x)? x-'A'+'a' : x)

/*
**
** getpath(): look up key in ascii sorted path database.
**
*/

/*	Every time reset to zero, path file is reopened */
long pathlength = 0;

getpath( key, path, cost)
char *key;		/* what we are looking for */
char *path;		/* where the path results go */
int *cost;		/* where the cost results go */
{
	long pos, middle, hi, lo;
	register char *s;
	int c;
	int flag;
	static FILE *file = (FILE *) NULL;

    DEBUG("getpath: looking for '%s'\n", key);

	if(pathlength == 0) {	/* open file on first use */
		if (file)
			(void) fclose(file);
		if((file = fopen(gpathdata, "r")) == NULL) {
			(void) printf("Can't access %s.\n", gpathdata);
			pathlength = -1;
		} else {
			(void) fseek(file, 0L, 2);	/* find length */
			pathlength = ftell(file);
		}
	}
	if( pathlength == -1 )
		return( 1 );

	lo = 0;
	hi = pathlength;
	(void) strcpy( path, key );
	(void) strcat( path, "\t" );
/*
** "Binary search routines are never written right the first time around."
** - Robert G. Sheldon.
*/
	for( ;; ) {
		pos = middle = ( hi+lo+1 )/2;
		(void) fseek(file, pos, 0);	/* find midpoint */
		if(pos != 0)
			while(((c = getc(file)) != EOF) && (c != '\n'))
				continue;	/* go to beginning of next line */
		if(c == EOF) {
			return(1);
		}
		for( flag = 0, s = path; flag == 0; s++ ) { /* match??? */
			if( *s == '\0' ) {
				goto solved;
			}
			if((c = getc(file)) == EOF) {
				return(1);
			}
			flag = lower(c) - lower(*s);
		} 
		if(lo >= middle) {		/* failure? */
			return(1);
		}
		if((c != EOF) && (flag < 0)) {	/* close window */
			lo = middle;
		} else {
			hi = middle - 1;
		}
	}
/* 
** Now just copy the result.
*/
solved:
	while(((c  = getc(file)) != EOF) && (c != '\t') && (c != '\n')) {
		*path++ = c;
	}
	*path = '\0';
/*
** See if the next field on the line is numeric.
** If so, use it as the cost for the route.
*/
	if(c == '\t') {
		int tcost = -1;
		while(((c = getc(file)) != EOF) && isdigit(c)) {
			if(tcost < 0) tcost = 0;
			tcost *= 10;
			tcost += c - '0';
		}
		if(tcost >= 0) *cost = tcost;
	}
	return (0);
}

