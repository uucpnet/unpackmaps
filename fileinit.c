/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.1 92/06/10
 */

#ifndef lint
static char SCCSid[] = "@(#)fileinit.c 1.1 92/06/10 01:16:15";
#endif

#include "unpack.h"

char *
makepath(dir, file)
char *dir, *file; {
    register char *p = (char *) malloc(strlen(dir) + strlen(file) + 10);
    char numbuf[10];
    fatal(!p, "Can't allocate temporary file names");
    (void) strcpy(p, dir);
    (void) strcat(p, "/");
    if (*file == '.') {
	(void) sprintf(numbuf, "%d", (int) getpid());
	(void) strcat(p, numbuf);
    }
    (void) strcat(p, file);
    return(p);
}
