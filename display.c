/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.3 92/08/15
 */

#ifndef lint
static char SCCSid[] = "@(#)display.c 1.3 92/08/15 22:31:35";
#endif
#include "unpack.h"

extern long pathlength;
extern char *gpathdata;
extern int verbose;

dumpmap(site)
register char *site; {
    char buf[BUFSIZ];
    register char *s,*e;
    int cost;

    pathlength = 0;
    gpathdata = wheredb;

    if (getpath(site, buf, &cost))
	return(1);

    s = buf;
    while(*s) {
	e = strchr(s, ':');
	if (e)
	    *e++ = '\0';
	(void) dumpit(s);
	if (e)
	    s = e;
	else
	    break;
    }
    return(0);
}

dumpit(mapfn)
char *mapfn; {

    char *p, *e;
    int offsets[20], *op = offsets;

    p = strchr(mapfn, ',');
    if (!p)
	return(1);
    *p++ = '\0';	/* mapfn now is map file name */
    while(*p) {
	e = strchr(p, ',');
	if (e)
	    *e = '\0';
	*op++ = atoi(p);
	if (e)
	    p = e + 1;
	else
	    break;
    }
    *op = 0;
    for (op = offsets; *op; op++)
	(void) printf("%s: %d\n", mapfn, *op);
    return(dumpfile(mapfn, offsets, 1));
}

dumpfile(mapfn, offsets, nterm)
char *mapfn;
int *offsets, nterm; {

    int compressed = 0, printing = 0;
    FILE *fin = (FILE *) NULL;
    register int *op = offsets;
    register int recno;
    char *((*rdfcn)());
    char *p;
    extern int rc;

    extern FILE *zfopen();
    extern char *zfgets();
    
    if (debug)
	(void) fprintf(stderr, "mapfn: %s\n", mapfn);

    if ((fin = fopen(mapfn, "r")) != NULL)
	rdfcn = fgets;
    else {
	(void) sprintf(tempbuf, "%s.Z", mapfn);
	fin = zfopen(tempbuf, "r");
	compressed = 1;
	rdfcn = zfgets;
    }

    if (!fin) {
	(void) fprintf(stderr, "%s: Can't open %s\n", progname, mapfn);
	rc |= 1;
	return(1);
    }

    recno = 0;
    while(rdfcn(tempbuf, sizeof(tempbuf), fin)) {
	recno++;

	if (!printing && recno == *op) {
	    printing = 1;
	}
	if (recno > *op && nterm && strncmp(tempbuf, "#N", 2) == 0) {
	    op++;
	    if (!*op)
		break;
	    if (recno < *op)
		printing = 0;
	}
	if (printing) {
	    if (verbose && tempbuf[0] == '#' &&
			   isupper(tempbuf[1]) &&
			   isspace(tempbuf[2])) {
		register char *str;
		str = "";
		switch(tempbuf[1]) {
		    case 'N': str = "Name:"; break;
		    case 'S': str = "System:"; break;
		    case 'O': str = "Organization:"; break;
		    case 'C': str = "Contacts:"; break;
		    case 'E': str = "Email:"; break;
		    case 'T': str = "Telephone:"; break;
		    case 'P': str = "Postal:"; break;
		    case 'L': str = "Lat/Long:"; break;
		    case 'R': str = "Remarks:"; break;
		    case 'U': str = "USENET links:"; break;
		    case 'W': str = "Who:"; break;
		}

		p = &tempbuf[1];

		if (!isspace(tempbuf[1]))
		    p++;

		while(*p && (*p == ' ' || *p == '\t')) p++;
		(void) printf("#%-15s%s", str, p);
	    } else
		(void) fputs(tempbuf, stdout);
	}
    }

    if (compressed)
	(void) zfclose(fin);
    else
	(void) fclose(fin);
    return(0);
}
