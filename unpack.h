/* Copyright 1992, Chris Lewis.  All Rights Reserved
   Please see the README for the terms of the copyright.
   1.5 92/11/28
 */

#ifndef lint
#ifdef INIT
static char head_SCCSid[] = "@(#)unpack.h 1.5 92/11/28 02:09:22";
#endif
#endif

#define VERSION "unpackmaps 4.1"

#include "config.h"
#include <stdio.h>
#include <errno.h>

extern int errno;


#ifdef	NEEDTYPES_H
#include <sys/types.h>
#endif

#include <sys/stat.h>

#ifdef	BSD
# include <strings.h>
# define strchr	index
# define strrchr rindex
# include <sys/file.h>

#else
# include <string.h>

# include <unistd.h>
#endif

#include <ctype.h>

#ifndef	F_OK
# define F_OK	0
# define X_OK	1
# define W_OK	2
# define R_OK	4
#endif

#ifndef	INIT
# define INIT(x)
# define EXTERN extern
#else
# define EXTERN
#endif

extern MALLRET *malloc();
extern MALLRET *realloc();

EXTERN char
	tempbuf[BUFSIZ],
	unpackdir[BUFSIZ]	INIT(MAPDIR),
	pathfile[BUFSIZ]	INIT(PATHFILE),
	*wheredb;

#ifdef	UNPACKMAPS
EXTERN char
	*togofile,
	*workfile,
	*tempfile,
	*wheretmp,
	*pathtmp,
	*pathtmp2,
	*notify			INIT((char *) NULL),
	*pathalias		INIT(PATHALIAS),
	compress[BUFSIZ]	INIT(COMPRESS),
	spooldir[BUFSIZ]	INIT(SPOOLDIR);
#endif	/* UNPACKMAPS */


struct stringlist {
    char **list;
    char **curptr;
    char **lastalloc;
};

#ifdef	UNPACKMAPS

EXTERN struct stringlist
		    mapfiles,
		    lmapfiles,
		    localmaps;

EXTERN char *internet		INIT((char*) NULL);
EXTERN int verbose;


#endif	/* UNPACKMAPS */

EXTERN int
	debug		INIT(0),
	usage		INIT(0);

#ifdef	UNPACKMAPS
EXTERN int
	runpath		INIT(0),
	forcepath	INIT(0),
	initialize	INIT(0),
	compflag	INIT(0),
	anyunpacked	INIT(0),
	whereonly	INIT(0),
	unlinkskip	INIT(0),
	unlinkflag	INIT(0),
	dontunpack	INIT(0),
	batchlock	INIT(0);
#endif	/* UNPACKMAPS */

extern FILE *popen();

EXTERN char *progname;

EXTERN char *makepath();
EXTERN int setbatch();
#ifdef USGDIR
#include <dirent.h>
#endif

#ifdef BERKDIR
#define	direct dirent
#include <sys/dir.h>
#endif
